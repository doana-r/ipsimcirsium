% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{map_questions}
\alias{map_questions}
\title{Creates radio buttons}
\usage{
map_questions(id_boite, langue)
}
\arguments{
\item{id_boite}{box id}

\item{langue}{lang (fr or en)}
}
\description{
Creates radio buttons
}
