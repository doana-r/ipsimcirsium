**IPSIM-Cirsium** is a tool developped by INRA for professionals in agriculture to predict and manage the damage caused by thistle on crops in France.

### User guide

1. Choose your language
1. Go to **your situation** tab
1. Tick what you want and validate
1. Explore model results
1. Go back to the questionnaire for testing new combinations...

**NOT UPDATED YET**

***
> Your data is used to produce the result and is not retained.

***
