
> ICI des explications sur le modèle

***

Le contenu écrit de cette application est disponible sous [licence CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) et le code sous [licence MIT](https://mit-license.org/) (© [DoAna](https://www.doana-r.com/) 2021) sauf mention contraire.


Ce site a été construit avec [shiny](https://shiny.rstudio.com/) et [R](https://www.r-project.org/).


Code source : https://gitlab.com/doana-r/ipsimcirsium
