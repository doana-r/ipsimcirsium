
> HERE model explanations

***

The written content of this app is available under the [CC-BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/) and its source code under the [MIT license](https://mit-license.org/) (© [DoAna](https://www.doana-r.com/) 2021) unless otherwise stated.


This website is built with [shiny](https://shiny.rstudio.com/) and [R](https://www.r-project.org/).


Source code : https://gitlab.com/doana-r/ipsimcirsium
