> **IPSIM-*****Cirsium*** est un modèle qui permet de tester l'efficacité de différentes options techniques pour gérer les infestations du chardon commun.


### Mode d'emploi

1. Choisissez votre langue en cliquant sur un drapeau au dessous des onglets
1. Cliquez l'onglet **Votre situation**
1. Renseignez les informations demandées et cliquez sur valider
1. Explorez les résultats du modèle
	- Le graphique du haut décrit l'évolution du niveau d'infestation du chardon. etc.
	- Le graphique du bas permet de détecter les causes les plus probables de l'infestation par le chardon d'après les résultats de votre questionnaire. etc.
1. Revenez-dans le questionnaire si vous souhaitez  tester de nouvelles combinaisons !


***

> - Ce modèle est générique et peut être utilisé pour une large gamme de conditions pédoclimatiques et de systèmes de culture. 
> Néanmoins, sa qualité de prédiction n'a été évaluée que sur un jeu de données français (Lacroix et al, 2021).
>
> - Vos données servent à produire le résultat et ne sont pas conservées.

***


