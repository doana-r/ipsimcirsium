# Météorologie

La connaissance des conditions météorologiques au printemps est nécessaire pour parvenir à estimer l'évolution de la présence de chardon dans votre parcelle.

La **température mensuelle** se calcule en faisant la moyenne des températures moyennes journalières sur chacun des mois de mars à mai en degrés Celcius.

La **pluviométrie** est la quantité totale de pluie en milimètres qui est tombée durant chacun des mois de mars à mai.

***

Si vous ne connaissez pas ces données, ceci devrait vous aider (dans le cas où votre parcelle se situerait en France métropolitaine):

<iframe id="meteo"
  title="doana-r.com/projets/meteo/"
  width="100%"
  height="600"
  src="https://www.doana-r.com/projets/meteo/">
</iframe>

*Source : [doana-r.com/projets/meteo/](https://www.doana-r.com/projets/meteo/)*  


Les deux cartes (Température et Pluviométrie) que vous voyez représentent les stations météo de référence de Météo-France. En cliquant sur la station la plus proche de votre parcelle, vous obtiendrez les températures et pluviométrie mensuelles sur les dernières années.

Il ne vous reste plus qu'à reporter les valeurs adéquates dans ce questionnaire.

<!--
HTML

<div id="container">
    <iframe id="embed" src="http://www.example.com"></iframe>
</div>

CSS

div#container
{
    width:840px;
    height:317px;
    overflow:scroll;     /* if you don't want a scrollbar, set to hidden */
    overflow-x:hidden;   /* hides horizontal scrollbar on newer browsers */

    /* resize and min-height are optional, allows user to resize viewable area */
    -webkit-resize:vertical; 
    -moz-resize:vertical;
    resize:vertical;
    min-height:317px;
}

iframe#embed
{
    width:1000px;       /* set this to approximate width of entire page you're embedding */
    height:2000px;      /* determines where the bottom of the page cuts off */
    margin-left:-183px; /* clipping left side of page */
    margin-top:-244px;  /* clipping top of page */
    overflow:hidden;

    /* resize seems to inherit in at least Firefox */
    -webkit-resize:none;
    -moz-resize:none;
    resize:none;
}
-->






