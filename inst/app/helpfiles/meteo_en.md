**A TRADUIRE**

# Météorologie

La connaissance des conditions météorologiques au printemps est nécessaire pour parvenir à estimer l'évolution de la présence de chardon dans votre parcelle.

La **température mensuelle** se calcule en faisant la moyenne des températures moyennes journalières sur chacun des mois de mars à mai en degrés Celcius.

La **pluviométrie** est la quantité totale de pluie en milimètres qui est tombée durant chacun des mois de mars à mai.

***

Si vous ne connaissez pas ces données, ceci devrait vous aider (dans le cas où votre parcelle se situerait en France métropolitaine):

<iframe id="meteo"
  title="doana-r.com/projets/meteo/"
  width="100%"
  height="600"
  src="https://www.doana-r.com/projets/meteo/">
</iframe>

*Source : [doana-r.com/projets/meteo/](https://www.doana-r.com/projets/meteo/)*  


Les deux cartes (Température et Pluviométrie) que vous voyez représentent les stations météo de référence de Météo-France. En cliquant sur la station la plus proche de votre parcelle, vous obtiendrez les températures et pluviométrie mensuelles sur les dernières années.

Il ne vous reste plus qu'à reporter les valeurs adéquates dans ce questionnaire.











